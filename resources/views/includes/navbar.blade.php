<!-- Navbar Start -->
<nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top p-0 wow fadeIn" data-wow-delay="0.1s">
    <a href="index.html" class="navbar-brand d-flex align-items-center px-4 px-lg-5">
        <h3 class="m-0 text-primary">Puskesmas Sekura</h3>
    </a>
    <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav ms-auto p-4 p-lg-0">
            <a href="{{ route('home') }}" class="nav-item nav-link active">Beranda</a>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Tentang Puskesmas</a>
                <div class="dropdown-menu rounded-0 rounded-bottom m-0">
                    <a href="#visimisi" class="dropdown-item">Visi & Misi</a>
                    <a href="struktur.html" class="dropdown-item">Struktur Organisasi</a>
                    <a href="tugaspokok.html" class="dropdown-item">Tugas Pokok dan Fungsi</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pelayanan</a>
                <div class="dropdown-menu rounded-0 rounded-bottom m-0">
                    <a href="#jenis" class="dropdown-item">Jenis Pelayanan</a>
                    <a href="#krisar" class="dropdown-item">Kritik & Saran</a>
                    <a href="pendaftaran.html" class="dropdown-item">Pendaftaran Online</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Info Kesehatan</a>
                <div class="dropdown-menu rounded-0 rounded-bottom m-0">
                     <a href="#artikel" class="dropdown-item">Artikel Kesehatan</a>
                    <a href="#" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#faqModal">FAQ</a>
                </div>
            </div>
            <a href="#kontak" class="nav-item nav-link">Kontak</a>
            <div>
                <a href="{{ route('login') }}" class="btn btn-primary rounded-pill">Sign In</a>
            </div>
        </div>
    </div>
</nav>
<!-- Navbar End -->

<!-- FAQ Modal -->
<div class="modal fade" id="faqModal" tabindex="-1" aria-labelledby="faqModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="faqModalLabel">Pertanyaan Umum</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                Apa saja layanan yang disediakan oleh Puskesmas?
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Puskesmas menyediakan berbagai layanan kesehatan dasar seperti pemeriksaan kesehatan rutin, konsultasi medis, vaksinasi, pengobatan umum, serta layanan kesehatan ibu dan anak.</div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                Bagaimana cara mendaftar sebagai pasien di Puskesmas?
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Anda dapat mendaftar sebagai pasien di Puskesmas dengan melakukan pendaftaran antrian online agar ketika datang ke Puskesmas bisa langsung mengisi formulir pendaftaran tanpa melakukan antrian yang lama <a href="{{ route('login') }}">Klik Disini</a> atau mengunjungi langsung puskesmas dengan mengambil nomor antrian terlebih dahulu dan mengisi formulir pendaftaran. Pastikan Anda membawa dokumen identitas yang diperlukan.</div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                Berapa biaya yang harus saya bayar untuk menggunakan layanan di Puskesmas?
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Puskesmas menyediakan layanan kesehatan dasar yang terjangkau atau bahkan gratis bagi masyarakat. Namun, biaya tertentu mungkin dikenakan untuk beberapa layanan atau obat-obatan tertentu.</div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFor" aria-expanded="false" aria-controls="flush-collapseFor">
                                Apa saja jam operasional Puskesmas?
                            </button>
                        </h2>
                        <div id="flush-collapseFor" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Jam operasional Puskesmas dapat bervariasi, namun umumnya Puskesmas buka dari pagi hingga sore hari, termasuk pada hari kerja. Pastikan Anda memeriksa jam operasional Puskesmas di lokasi Anda sebelum mengunjunginya.</div>
                        </div>
                    </div>

                

            
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
