@extends('layouts.app')

@section('title')
Puskesmas Sekura
@endsection

@section('content')
<!-- Header Start -->
<div class="container-fluid header bg-primary p-0 mb-5">
    <div class="row g-0 align-items-center flex-column-reverse flex-lg-row">
        <div class="col-lg-6 p-5 wow fadeIn" data-wow-delay="0.1s">
            <h1 class="text-white">Selamat datang di <br> Puskesmas Sekura!</h1>
            <h5 class="text-white mb-3">Pendaftaran antrian online menjadi lebih mudah dan nyaman!</h5>
            <a class="btn rounded-pill py-3 px-5" style="background-color: white; color: black;"
                href="{{ route('login') }}">Klik Disini</a>
        </div>
        <div class="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
            <div class="owl-carousel header-carousel">
                <div class="owl-carousel-item position-relative">
                    <img class="img-fluid" src="img/cr-1.png" alt="">
                    <div class="owl-carousel-text">
                        <h1 class="display-1 text-white mb-0">Cardiology</h1>
                    </div>
                </div>
                <div class="owl-carousel-item position-relative">
                    <img class="img-fluid" src="img/cr-3.jpg" alt="">
                    <div class="owl-carousel-text">
                        <h1 class="display-1 text-white mb-0">Neurology</h1>
                    </div>
                </div>
                <div class="owl-carousel-item position-relative">
                    <img class="img-fluid" src="img/cr-2.jpg" alt="">
                    <div class="owl-carousel-text">
                        <h1 class="display-1 text-white mb-0">Pulmonary</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Header End -->




<!-- About Start -->
<div class="container-xxl py-5">
    <div class="container">
        <div class="row g-5">
            <div class="col-lg-6 wow fadeIn" data-wow-delay="0.1s">
                <div class="d-flex flex-column">
                    <img class="img-fluid rounded w-75 align-self-end" src="img/about4.jpg" alt="">
                    <img class="img-fluid rounded w-50 bg-white pt-3 pe-3" src="img/about3.jpg" alt=""
                        style="margin-top: -25%;">
                </div>
            </div>
            <div class="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
                <p class="d-inline-block border rounded-pill py-1 px-4">Tentang Kami</p>
                <h1 class="mb-4">Kenali Tentang Kami!</h1>
                <p>Puskesmas adalah Unit Pelaksana Teknis Dinas Kesehatan Kabupaten yang bertanggungjawab
                    menyelenggarakan pembangunan kesehatan di suatu wilayah kerja tertantu. Puskesmas berfungsi sebagai:
                </p>
                <p class="mb-4">Puskesmas berfungsi sebagai (1) Pusat penggerak pembangunan berwawasan kesehatan ; (2)
                    Pusat pemberdayaan keluarga dan masyarakat ; (3) Pusat Pelayanan kesehatan strata pertama. </p>
                <p><i class="far fa-check-circle text-primary me-3"></i>P1. Perencanaan</p>
                <p><i class="far fa-check-circle text-primary me-3"></i>P2. Penggerakan, Pelaksanaan</p>
                <p><i class="far fa-check-circle text-primary me-3"></i>P3. Pengawasan, Pengendalian, Penilaian</p>
                <a class="btn btn-primary rounded-pill py-3 px-5 mt-3" href="">Read More</a>
            </div>
        </div>
    </div>
</div>
<!-- About End -->


<!-- Service Start -->
<div class="container-xxl py-5" id="jenis">
    <div class="container">
        <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
            <p class="d-inline-block border rounded-pill py-1 px-4">Melayani</p>
            <h1>Layanan Medis Kami</h1>
        </div>
        <div class="row g-4">
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="service-item bg-light rounded h-100 p-5">
                    <div class="d-inline-flex align-items-center justify-content-center bg-white rounded-circle mb-4"
                        style="width: 65px; height: 65px;">
                        <i class="fa fa-heartbeat text-primary fs-4"></i>
                    </div>
                    <h4 class="mb-3">Rawat Inap</h4>
                    <p class="mb-4">Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam
                        sed stet.</p>
                    <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                <div class="service-item bg-light rounded h-100 p-5">
                    <div class="d-inline-flex align-items-center justify-content-center bg-white rounded-circle mb-4"
                        style="width: 65px; height: 65px;">
                        <i class="fa fa-x-ray text-primary fs-4"></i>
                    </div>
                    <h4 class="mb-3">Rawat Jalan</h4>
                    <p class="mb-4">Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam
                        sed stet.</p>
                    <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <div class="service-item bg-light rounded h-100 p-5">
                    <div class="d-inline-flex align-items-center justify-content-center bg-white rounded-circle mb-4"
                        style="width: 65px; height: 65px;">
                        <i class="fa fa-brain text-primary fs-4"></i>
                    </div>
                    <h4 class="mb-3">IGD</h4>
                    <p class="mb-4">Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam
                        sed stet.</p>
                    <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="service-item bg-light rounded h-100 p-5">
                    <div class="d-inline-flex align-items-center justify-content-center bg-white rounded-circle mb-4"
                        style="width: 65px; height: 65px;">
                        <i class="fa fa-wheelchair text-primary fs-4"></i>
                    </div>
                    <h4 class="mb-3">Orthopedics</h4>
                    <p class="mb-4">Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam
                        sed stet.</p>
                    <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                <div class="service-item bg-light rounded h-100 p-5">
                    <div class="d-inline-flex align-items-center justify-content-center bg-white rounded-circle mb-4"
                        style="width: 65px; height: 65px;">
                        <i class="fa fa-tooth text-primary fs-4"></i>
                    </div>
                    <h4 class="mb-3">Dental Surgery</h4>
                    <p class="mb-4">Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam
                        sed stet.</p>
                    <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <div class="service-item bg-light rounded h-100 p-5">
                    <div class="d-inline-flex align-items-center justify-content-center bg-white rounded-circle mb-4"
                        style="width: 65px; height: 65px;">
                        <i class="fa fa-vials text-primary fs-4"></i>
                    </div>
                    <h4 class="mb-3">Laboratory</h4>
                    <p class="mb-4">Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam
                        sed stet.</p>
                    <a class="btn" href=""><i class="fa fa-plus text-primary me-3"></i>Read More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Service End -->


<!-- Tata Nilai Start -->
<div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
    <p class="d-inline-block border rounded-pill py-1 px-4">Puskesmas Sekura</p>
    <h1>Tata Nilai</h1>
</div>
<div class="container-fluid bg-primary overflow-hidden my-5 px-lg-0">

    <div class="container tatanilai px-lg-0">

        <div class="row g-0 mx-lg-0">
            <div class="col-lg-6 tatanilai-text py-5 wow fadeIn" data-wow-delay="0.1s">
                <div class="p-lg-5 ps-lg-0">
                    <p class="d-inline-block border rounded-pill text-light py-1 px-4">Tata Nilai</p>
                    <div class="row g-4">
                        <div class="col-6">
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center rounded-circle bg-light"
                                    style="width: 55px; height: 55px;">
                                    <i class="fa fa-check text-primary"></i>
                                </div>
                                <div class="ms-4">

                                    <h5 class="text-white mb-0">Senyum</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center rounded-circle bg-light"
                                    style="width: 55px; height: 55px;">
                                    <i class="fa fa-check text-primary"></i>
                                </div>
                                <div class="ms-4">

                                    <h5 class="text-white mb-0">Edukatif</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center rounded-circle bg-light"
                                    style="width: 55px; height: 55px;">
                                    <i class="fa fa-check text-primary"></i>
                                </div>
                                <div class="ms-4">

                                    <h5 class="text-white mb-0">Komitmen</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center rounded-circle bg-light"
                                    style="width: 55px; height: 55px;">
                                    <i class="fa fa-check text-primary"></i>
                                </div>
                                <div class="ms-4">

                                    <h5 class="text-white mb-0">Unggul</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center rounded-circle bg-light"
                                    style="width: 55px; height: 55px;">
                                    <i class="fa fa-check text-primary"></i>
                                </div>
                                <div class="ms-4">

                                    <h5 class="text-white mb-0">Religius</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex align-items-center">
                                <div class="d-flex flex-shrink-0 align-items-center justify-content-center rounded-circle bg-light"
                                    style="width: 55px; height: 55px;">
                                    <i class="fa fa-check text-primary"></i>
                                </div>
                                <div class="ms-4">

                                    <h5 class="text-white mb-0">Akuntabel</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 pe-lg-0 wow fadeIn" data-wow-delay="0.5s" style="min-height: 400px;">
                <div class="position-relative h-100">
                    <img class="position-absolute img-fluid w-100 h-100" src="img/about1.jpg" style="object-fit: cover;"
                        alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tata Nilai End -->

<!-- Visi Misi -->
<div class="container-xxl py-5" id="visimisi">
    <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
        <p class="d-inline-block border rounded-pill py-1 px-4">Puskesmas Sekura</p>
        <h1>Visi Misi</h1>
    </div>
    <div class="row justify-content-center mb-5">
        <div class="col-md-6 mb-4 mb-lg-0 col-lg-4 aos-init" data-aos="fade-up" data-aos-delay="200">
            <div class="card shadow">
                <div class="card-body">
                    <h3 class="card-title text-center mb-4">VISI</h3>
                    <ul class="list-unstyled">
                        <li><i class="fas fa-check text-primary"></i> Menjadi Pusat Pelayanan Kesehatan yang Mandiri,
                            Bermutu dan Berakhlakul Karimah Menuju Kecamatan Teluk Keramat Sehat</li>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />

                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-6 mb-4 mb-lg-0 col-lg-4 aos-init" data-aos="fade-up" data-aos-delay="200">
            <div class="card shadow">
                <div class="card-body">
                    <h3 class="card-title text-center mb-4">MISI</h3>
                    <ul class="list-unstyled">
                        <li><i class="fas fa-check text-primary"></i> Mendorong kesadaran masyarakat supaya berperilaku
                            hidup bersih dan sehat secara mandiri</li>
                        <li><i class="fas fa-check text-primary"></i> Menjadikan Puskesmas sebagai pusat informasi dan
                            pelayanan kesehatan</li>
                        <li><i class="fas fa-check text-primary"></i> Meningkatkan profesionalisme tenaga kesehatan,
                            secara berkelanjutan sesuai dengan kompetensi masing-masing petugas kesehatan</li>
                        <li><i class="fas fa-check text-primary"></i> Memelihara dan meningkatkan derajat kesehatan
                            individu, keluarga, kelompok masyarakat dan lingkungan</li>
                        <li><i class="fas fa-check text-primary"></i> Menjalin dan meningkatkan kemitraan internal dan
                            eksternal dalam rangka mendukung terjadinya peningkatan derajat kesehatan masyarakat secara
                            berkesinambungan</li>
                        <li><i class="fas fa-check text-primary"></i> Meningkatkan kualitas pelayanan yang bermutu,
                            terstandarisasi, serta terjangkau oleh masyarakat</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--End Visi Misi-->

<!-- Team Start -->
<div class="container-xxl py-5">
    <div class="container">
        <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
            <p class="d-inline-block border rounded-pill py-1 px-4">Puskesmas Sekura</p>
            <h1>Dokter</h1>
        </div>
        <div class="row g-4">
            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="team-item position-relative rounded overflow-hidden">
                    <div class="overflow-hidden">
                        <img class="img-fluid" src="img/team-1.jpg" alt="">
                    </div>
                    <div class="team-text bg-light text-center p-4">
                        <h5>Nama</h5>
                        <p class="text-primary">Departmen</p>
                        <div class="team-social text-center">
                            <a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                <div class="team-item position-relative rounded overflow-hidden">
                    <div class="overflow-hidden">
                        <img class="img-fluid" src="img/team-2.jpg" alt="">
                    </div>
                    <div class="team-text bg-light text-center p-4">
                        <h5>Nama</h5>
                        <p class="text-primary">Departmen</p>
                        <div class="team-social text-center">
                            <a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <div class="team-item position-relative rounded overflow-hidden">
                    <div class="overflow-hidden">
                        <img class="img-fluid" src="img/team-3.jpg" alt="">
                    </div>
                    <div class="team-text bg-light text-center p-4">
                        <h5>Nama</h5>
                        <p class="text-primary">Departmen</p>
                        <div class="team-social text-center">
                            <a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.7s">
                <div class="team-item position-relative rounded overflow-hidden">
                    <div class="overflow-hidden">
                        <img class="img-fluid" src="img/team-4.jpg" alt="">
                    </div>
                    <div class="team-text bg-light text-center p-4">
                        <h5>Nama</h5>
                        <p class="text-primary">Departmen</p>
                        <div class="team-social text-center">
                            <a class="btn btn-square" href=""><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-square" href=""><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-square" href=""><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Team End -->

<!-- Artikel Start -->
<div class="container-xxl py-5" id="artikel">
    <div class="container">
        <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
            <p class="d-inline-block border rounded-pill py-1 px-4">Artikel</p>
            <h1>Artikel Kesehatan</h1>
        </div>
        <div class="owl-carousel testimonial-carousel wow fadeInUp" data-wow-delay="0.1s">
            <div class="testimonial-item text-center">
                <img class="img-fluid bg-light rounded-circle p-2 mx-auto mb-4" src="img/artikel-1.jpg"
                    style="width: 100px; height: 100px;">
                <div class="testimonial-text rounded text-center p-4">
                    <h5 class="mb-1">Manfaat Minum Air Putih Secara Teratur</h5>
                    <p>Tubuh yang terhidrasi dengan baik dapat mendukung fungsi otak yang optimal. Minum air putih yang
                        cukup dapat membantu meningkatkan konsentrasi, kejelasan pikiran, dan kinerja mental.</p>

                </div>
            </div>
            <div class="testimonial-item text-center">
                <img class="img-fluid bg-light rounded-circle p-2 mx-auto mb-4" src="img/artikel-3.jpg"
                    style="width: 100px; height: 100px;">
                <div class="testimonial-text rounded text-center p-4">
                    <h5 class="mb-1">Tips untuk Menjaga Kesehatan Tubuh Anda</h5>
                    <p>Istirahat yang cukup akan membantu tubuh Anda pulih, meningkatkan sistem kekebalan tubuh, dan
                        menjaga keseimbangan emosi.</p>
                </div>
            </div>
            <div class="testimonial-item text-center">
                <img class="img-fluid bg-light rounded-circle p-2 mx-auto mb-4" src="img/artikel-2.jpg"
                    style="width: 100px; height: 100px;">
                <div class="testimonial-text rounded text-center p-4">
                    <h5 class="mb-1">Cara Mengurangi Stres</h5>
                    <p>Meditasi, pernapasan dalam-dalam, atau teknik relaksasi lainnya dapat membantu menenangkan
                        pikiran dan tubuh Anda. Sediakan waktu beberapa menit setiap hari untuk duduk dengan tenang,
                        mengamati pernapasan, dan membiarkan pikiran-pikiran stres mengalir pergi.</p>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Artikel End -->


<!-- Contact Start -->
<div class="container-xxl py-5" id="kontak">
    <div class="container">
        <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
            <p class="d-inline-block border rounded-pill py-1 px-4">Puskesmas Sekura</p>
            <h1>Kontak Kami</h1>
        </div>
        <div class="row g-4">
            <div class="col-lg-4">
                <div class="h-100 bg-light rounded d-flex align-items-center p-5">
                    <div class="d-flex flex-shrink-0 align-items-center justify-content-center rounded-circle bg-white"
                        style="width: 55px; height: 55px;">
                        <i class="fa fa-map-marker-alt text-primary"></i>
                    </div>
                    <div class="ms-4">
                        <p class="mb-2">Alamat</p>
                        <h5 class="mb-0">Jl.Kesehatan No 41, Sekura, 79465</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="h-100 bg-light rounded d-flex align-items-center p-5">
                    <div class="d-flex flex-shrink-0 align-items-center justify-content-center rounded-circle bg-white"
                        style="width: 55px; height: 55px;">
                        <i class="fa fa-phone-alt text-primary"></i>
                    </div>
                    <div class="ms-4">
                        <p class="mb-2">Telepon</p>
                        <h5 class="mb-0">0811-5722-456</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="h-100 bg-light rounded d-flex align-items-center p-5">
                    <div class="d-flex flex-shrink-0 align-items-center justify-content-center rounded-circle bg-white"
                        style="width: 55px; height: 55px;">
                        <i class="fa fa-envelope-open text-primary"></i>
                    </div>
                    <div class="ms-4">
                        <p class="mb-2">Email</p>
                        <h5 class="mb-0">pkm.sekura@gmail.com</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow fadeIn" data-wow-delay="0.1s">
                <div class="bg-light rounded p-5">
                    <p class="d-inline-block border rounded-pill py-1 px-4">Contact Us</p>
                    <h1 class="mb-4">Punya Pertanyaan? Silahkan Hubungi Kami!</h1>

                    <form>
                        <div class="row g-3">
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="name" placeholder="Nama">
                                    <label for="name">Nama</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="email" class="form-control" id="email" placeholder="Email">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating">
                                    <input type="text" class="form-control" id="subject" placeholder="Subject">
                                    <label for="subject">Subject</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-floating">
                                    <textarea class="form-control" placeholder="Leave a message here" id="message"
                                        style="height: 100px"></textarea>
                                    <label for="message">Message</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-primary w-100 py-3" type="submit">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
                <div class="h-100" style="min-height: 400px;">
                    <iframe class="rounded w-100 h-100"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.5013558711094!2d109.2193878742217!3d1.4721770611719955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31e4c04d34165fb5%3A0xd4f2cb54da1b75b4!2sPuskesmas%20Sekura!5e0!3m2!1sid!2sid!4v1688981913737!5m2!1sid!2sid"
                        frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact End -->


<!--Kritik dan Saran -->
<div class="container" id="krisar">
    <div class="row" id="saran-section">
        <div class="col-md-12 mb-5">
            <form action="" method="post" class="p-5 bg-white">
                <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
                    <p class="d-inline-block border rounded-pill py-1 px-4">Puskesmas Sekura</p>
                    <h1>Kritik & Saran</h1>
                </div>
                <div class="row form-group">
                    <div class="col-md-6 mb-3 mb-md-0">
                        <label for="name" class="text-black ">First Name</label>
                        <input type="text" id="name" name="name" class="form-control rounded-pill" />
                    </div>
                    <div class="col-md-6">
                        <label for="1name" class="text-black ">Last Name</label>
                        <input type="text" id="1name" name="1name" class="form-control rounded-pill" />
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <label for="email" class="tetx-black mt-2">Email</label>
                        <input type="text" id="email" name="email" class="form-control rounded-pill" />
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <label for="subject" class="text-black mt-2">Subject</label>
                        <input type="text" id="subject" name="subject" class="form-control rounded-pill" />
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <label for="message" class="text-black mt-2">Message</label>
                        <input type="text" id="message" name="message" col="30" rows="7"
                            class="form-control rounded-pill" placeholder="Tulis Pesan Anda Disini..." />
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12 mt-3">
                        <button type="submit" class="btn btn-primary btn-md text-white mb-5 rounded-pill">
                            send message
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--Kritik dan Saran End-->



@endsection
@push('addon-style')
<style>
    .btn:hover {
        background-color: background-color: #ddd;
         !important;
        color: #ddd !important;
    }

</style>
@endpush
